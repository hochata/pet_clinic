alias PetClinic.Repo

alias PetClinic.PetClinicService.{Pet, PetHealthExpert, Owner, Species}

alias PetClinic.AppointmentService.{Appointment, ExpertSchedule}
