defmodule PetClinicWeb.SpecialityControllerTest do
  use PetClinicWeb.ConnCase

  import PetClinic.PetClinicServiceFixtures

  @create_attrs %{expert_id: 0, species_id: 0}
  @update_attrs %{expert_id: 1, species_id: 1}
  @invalid_attrs %{expert_id: nil, species_id: nil}

  describe "index" do
    test "lists all specialities", %{conn: conn} do
      conn = get(conn, Routes.speciality_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Specialities"
    end
  end

  describe "new speciality" do
    @tag :skip
    test "renders form", %{conn: conn} do
      conn = get(conn, Routes.speciality_path(conn, :new))
      assert html_response(conn, 200) =~ "New Speciality"
    end
  end

  describe "create speciality" do
    @tag :skip
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, Routes.speciality_path(conn, :create), speciality: @create_attrs)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.speciality_path(conn, :show, id)

      conn = get(conn, Routes.speciality_path(conn, :show, id))
      assert html_response(conn, 200) =~ "Show Speciality"
    end

    @tag :skip
    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.speciality_path(conn, :create), speciality: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Speciality"
    end
  end

  describe "edit speciality" do
    setup [:create_speciality]

    @tag :skip
    test "renders form for editing chosen speciality", %{conn: conn, speciality: speciality} do
      conn = get(conn, Routes.speciality_path(conn, :edit, speciality))
      assert html_response(conn, 200) =~ "Edit Speciality"
    end
  end

  describe "update speciality" do
    setup [:create_speciality]

    @tag :skip
    test "redirects when data is valid", %{conn: conn, speciality: speciality} do
      conn =
        put(conn, Routes.speciality_path(conn, :update, speciality), speciality: @update_attrs)

      assert redirected_to(conn) == Routes.speciality_path(conn, :show, speciality)

      conn = get(conn, Routes.speciality_path(conn, :show, speciality))
      assert html_response(conn, 200)
    end

    @tag :skip
    test "renders errors when data is invalid", %{conn: conn, speciality: speciality} do
      conn =
        put(conn, Routes.speciality_path(conn, :update, speciality), speciality: @invalid_attrs)

      assert html_response(conn, 200) =~ "Edit Speciality"
    end
  end

  describe "delete speciality" do
    setup [:create_speciality]

    test "deletes chosen speciality", %{conn: conn, speciality: speciality} do
      conn = delete(conn, Routes.speciality_path(conn, :delete, speciality))
      assert redirected_to(conn) == Routes.speciality_path(conn, :index)

      assert_error_sent 404, fn ->
        get(conn, Routes.speciality_path(conn, :show, speciality))
      end
    end
  end

  defp create_speciality(_) do
    speciality = speciality_fixture()
    %{speciality: speciality}
  end
end
