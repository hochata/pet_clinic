defmodule PetClinic.PetClinicServiceFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `PetClinic.PetClinicService` context.
  """

  @doc """
  Generate a pet_health_expert.
  """
  def pet_health_expert_fixture(attrs \\ %{}) do
    {:ok, pet_health_expert} =
      attrs
      |> Enum.into(%{
        age: 42,
        name: "some name",
        sex: :female
      })
      |> PetClinic.PetClinicService.create_pet_health_expert()

    pet_health_expert
  end

  @doc """
  Generate a pet.
  """
  def pet_fixture(attrs \\ %{}) do
    species = species_fixture()
    owner = owner_fixture()

    {:ok, pet} =
      attrs
      |> Enum.into(%{
        age: 42,
        name: "some name",
        sex: :male,
        species_id: species.id,
        owner_id: owner.id
      })
      |> PetClinic.PetClinicService.create_pet()

    pet
  end

  @doc """
  Generate a owner.
  """
  def owner_fixture(attrs \\ %{}) do
    {:ok, owner} =
      attrs
      |> Enum.into(%{
        age: 42,
        email: "some email",
        name: "some name",
        phone_num: "some phone_num"
      })
      |> PetClinic.PetClinicService.create_owner()

    owner
  end

  @doc """
  Generate a speciality.
  """
  def speciality_fixture(attrs \\ %{}) do
    species = species_fixture()
    expert = pet_health_expert_fixture()

    {:ok, speciality} =
      attrs
      |> Enum.into(%{
        expert_id: expert.id,
        species_id: species.id
      })
      |> PetClinic.PetClinicService.create_speciality()

    speciality
  end

  def species_fixture(attrs \\ %{}) do
    {:ok, species} =
      attrs
      |> Enum.into(%{
        name: "crocodile"
      })
      |> PetClinic.PetClinicService.create_species()

    species
  end
end
