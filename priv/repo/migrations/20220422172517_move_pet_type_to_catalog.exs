defmodule PetClinic.Repo.Migrations.MovePetTypeToCatalog do
  use Ecto.Migration

  alias PetClinic.Repo
  alias PetClinic.PetClinicService.Species
  alias PetClinic.PetClinicService.Pet

  import Ecto.Query

  def down do
    pets =
      "pets"
      |> join(:inner, [p], s in "species", on: p.species_id == s.id)
      |> select([p, s], %{id: p.id, species: s.name})
      |> Repo.all()

    alter table(:pets) do
      remove :species_id
      add :type, :string
    end

    flush()

    IO.inspect(pets)

    Enum.each(pets, fn pet ->
      update = "UPDATE pets SET type = $1 WHERE id = $2"
      Repo.query!(update, [pet.species, pet.id])
    end)

    drop table(:species)
  end

  def up do
    pets =
      from(p in "pets", select: %{type: p.type, id: p.id})
      |> Repo.all()
      |> Enum.map(fn pet ->
        %{pet | type: pet.type |> String.downcase()}
      end)

    pet_types = pets |> Enum.map(& &1.type) |> Enum.uniq()

    create table(:species) do
      add :name, :string
      timestamps()
    end

    flush()

    Enum.map(pet_types, fn type ->
      Repo.insert(%Species{name: type})
    end)

    alter table(:pets) do
      remove :type
      add :species_id, references(:species)
    end

    flush()

    Enum.map(pets, fn pet ->
      %Species{id: sp_id} = Species |> Repo.get_by(name: pet.type)

      q = "UPDATE pets SET species_id = $1 WHERE id = $2;"

      Repo.query!(q, [sp_id, pet.id])
    end)
  end
end
