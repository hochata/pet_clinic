defmodule PetClinic.Repo.Migrations.NormalizePetSex do
  use Ecto.Migration

  alias PetClinic.Repo

  import Ecto.Query

  def change do
    pets = from(p in "pets", select: %{id: p.id, sex: p.sex}) |> Repo.all()

    Enum.each(pets, fn pet ->
      update = "UPDATE pets SET sex = $1 WHERE id = $2"
      Repo.query!(update, [pet.sex |> String.downcase(), pet.id])
    end)
  end
end
