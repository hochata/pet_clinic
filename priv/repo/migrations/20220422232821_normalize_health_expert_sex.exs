defmodule PetClinic.Repo.Migrations.NormalizeHealthExpertSex do
  use Ecto.Migration

  alias PetClinic.Repo

  import Ecto.Query

  defp normalize_sex(sex) do
    case String.downcase(sex) do
      s when s in ["male", "female"] ->
        s

      _ ->
        "female"
    end
  end

  def change do
    vets =
      from(v in "pet_health_experts", select: %{id: v.id, sex: v.sex})
      |> Repo.all()

    Enum.each(vets, fn v ->
      update = "UPDATE pet_health_experts SET sex = $1 WHERE id = $2"
      Repo.query!(update, [normalize_sex(v.sex), v.id])
    end)
  end
end
