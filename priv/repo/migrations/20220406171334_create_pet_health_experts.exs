defmodule PetClinic.Repo.Migrations.CreatePetHealthExperts do
  use Ecto.Migration

  def change do
    create table(:pet_health_experts) do
      add :name, :string
      add :age, :integer
      add :specialities, :string
      add :sex, :string

      timestamps()
    end
  end
end
