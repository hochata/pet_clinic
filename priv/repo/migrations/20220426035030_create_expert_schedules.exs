defmodule PetClinic.Repo.Migrations.CreateExpertSchedules do
  use Ecto.Migration

  def change do
    create table(:expert_schedules) do
      add :end_hour, :time
      add :start_hour, :time
      add :start_date, :date
      add :end_date, :date
      add :expert_id, references(:pet_health_experts)

      timestamps()
    end
  end
end
