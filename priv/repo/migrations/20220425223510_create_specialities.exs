defmodule PetClinic.Repo.Migrations.CreateSpecialities do
  use Ecto.Migration

  import Ecto.Query

  alias PetClinic.PetClinicService.Speciality
  alias PetClinic.Repo

  def up do
    experts =
      "pet_health_experts"
      |> select([e], %{id: e.id, specialities: e.specialities})
      |> Repo.all()
      |> Enum.map(fn e ->
        %{
          e
          | specialities:
              e.specialities
              |> String.split(",")
              |> Stream.map(&String.trim/1)
              |> Enum.map(&String.downcase/1)
        }
      end)
      |> IO.inspect()

    create table(:specialities) do
      add :expert_id, references(:pet_health_experts), primary_key: true
      add :species_id, references(:species), primary_key: true

      timestamps()
    end

    flush()

    experts
    |> Enum.each(fn e ->
      Enum.each(e.specialities, fn sp ->
        sp_id =
          from(s in "species", where: s.name == ^sp, select: s.id)
          |> Repo.one!()

        Repo.insert(%Speciality{expert_id: e.id, species_id: sp_id})
      end)
    end)

    alter table(:pet_health_experts) do
      remove :specialities
    end
  end

  def down do
    experts =
      "specialities"
      |> join(:inner, [s], sp in "species", on: s.species_id == sp.id)
      |> group_by([s, _], s.expert_id)
      |> select(
        [s, sp],
        %{
          id: s.expert_id,
          specialities: fragment("STRING_AGG(?, ',')", sp.name)
        }
      )
      |> Repo.all()

    IO.inspect(experts)

    alter table(:pet_health_experts) do
      add :specialities, :string
    end

    flush()

    Enum.each(experts, fn e ->
      q = "UPDATE pet_health_experts SET specialities = $1 WHERE id = $2;"
      Repo.query!(q, [e.specialities, e.id])
    end)

    drop table(:specialities)
  end
end
