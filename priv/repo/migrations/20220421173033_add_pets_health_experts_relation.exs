defmodule PetClinic.Repo.Migrations.AddPetsHealthExpertsRelation do
  use Ecto.Migration

  def change do
    alter table(:pets) do
      add :preferred_expert_id, references(:pet_health_experts)
    end
  end
end
