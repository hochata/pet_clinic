defmodule PetClinic.PetClinicService.Pet do
  @moduledoc false

  use Ecto.Schema
  import Ecto.Changeset

  alias PetClinic.PetClinicService.{Owner, PetHealthExpert, Species}
  alias PetClinic.AppointmentService.{Appointment}

  schema "pets" do
    field :age, :integer
    field :name, :string
    field :sex, Ecto.Enum, values: [:female, :male]
    belongs_to :species, Species
    belongs_to :owner, Owner
    belongs_to :preferred_expert, PetHealthExpert

    has_many :appointments, Appointment

    timestamps()
  end

  @doc false
  def changeset(pet, attrs) do
    pet
    |> cast(
      attrs,
      [:name, :age, :species_id, :owner_id, :preferred_expert_id, :sex]
    )
    |> validate_required([:name, :age, :species_id, :owner_id, :sex])
    |> validate_number(:age, greater_than_or_equal_to: 0)
  end
end
