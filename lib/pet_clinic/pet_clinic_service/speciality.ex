defmodule PetClinic.PetClinicService.Speciality do
  @moduledoc false

  use Ecto.Schema
  import Ecto.Changeset

  alias PetClinic.PetClinicService.{PetHealthExpert, Species}

  schema "specialities" do
    belongs_to :expert, PetHealthExpert
    belongs_to :species, Species

    timestamps()
  end

  @doc false
  def changeset(speciality, attrs) do
    speciality
    |> cast(attrs, [:expert_id, :species_id])
    |> validate_required([:expert_id, :species_id])
  end
end
