defmodule PetClinic.PetClinicService.Species do
  @moduledoc false

  use Ecto.Schema
  import Ecto.Changeset

  schema "species" do
    field :name, :string

    timestamps()
  end

  @doc false
  def changeset(pet, attrs) do
    pet
    |> cast(attrs, [:name])
    |> validate_required([:name])
  end
end
