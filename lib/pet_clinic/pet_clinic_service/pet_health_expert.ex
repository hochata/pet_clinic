defmodule PetClinic.PetClinicService.PetHealthExpert do
  @moduledoc false

  use Ecto.Schema
  import Ecto.Changeset

  alias PetClinic.PetClinicService
  alias PetClinic.PetClinicService.{Pet, Speciality, Species}
  alias PetClinic.AppointmentService.{Appointment, ExpertSchedule}

  schema "pet_health_experts" do
    field :age, :integer
    field :name, :string
    field :sex, Ecto.Enum, values: [:female, :male]

    many_to_many(
      :specialities,
      Species,
      join_through: Speciality,
      join_keys: [expert_id: :id, species_id: :id]
    )

    has_many(:patients, Pet, foreign_key: :preferred_expert_id)
    has_many(:appointments, Appointment, foreign_key: :expert_id)
    has_one :schedule, ExpertSchedule, foreign_key: :expert_id

    timestamps()
  end

  def maybe_put_specialities(chset, attrs) do
    if attrs["specialities"] do
      specialities =
        Enum.map(attrs["specialities"], fn sp ->
          with {n, _rest} <- Integer.parse(sp) do
            PetClinicService.get_species!(n)
          end
        end)

      put_assoc(chset, :specialities, specialities)
    else
      chset
    end
  end

  @doc false
  def changeset(pet_health_expert, attrs) do
    pet_health_expert
    |> cast(attrs, [:name, :age, :sex])
    |> validate_required([:name, :age, :sex])
    |> maybe_put_specialities(attrs)
  end
end
