defmodule PetClinic.AppointmentService do
  @moduledoc """
  The AppointmentService context.
  """

  import Ecto.Query, warn: false
  alias PetClinic.Repo

  alias PetClinic.AppointmentService.{Appointment, ExpertSchedule}
  alias PetClinic.PetClinicService.{Pet}

  @doc """
  Returns the list of appointments.

  ## Examples

      iex> list_appointments()
      [%Appointment{}, ...]

  """
  def list_appointments do
    Repo.all(Appointment)
  end

  def list_appointments(preloads: pr) do
    Repo.all(Appointment)
    |> Repo.preload(pr)
  end

  @doc """
  Gets a single appointment.

  Raises `Ecto.NoResultsError` if the Appointment does not exist.

  ## Examples

      iex> get_appointment!(123)
      %Appointment{}

      iex> get_appointment!(456)
      ** (Ecto.NoResultsError)

  """
  def get_appointment!(id), do: Repo.get!(Appointment, id)

  def get_appointments_by(%{expert_id: expert_id, date: date}, preloads: pr) do
    Repo.all(
      from a in Appointment,
        where: a.expert_id == ^expert_id,
        where: fragment("?::date = ?", a.datetime, ^date)
    )
    |> Repo.preload(pr)
  end

  @doc """
  Creates a appointment.

  ## Examples

      iex> create_appointment(%{field: value})
      {:ok, %Appointment{}}

      iex> create_appointment(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_appointment(attrs \\ %{}) do
    %Appointment{}
    |> Appointment.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a appointment.

  ## Examples

      iex> update_appointment(appointment, %{field: new_value})
      {:ok, %Appointment{}}

      iex> update_appointment(appointment, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_appointment(%Appointment{} = appointment, attrs) do
    appointment
    |> Appointment.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a appointment.

  ## Examples

      iex> delete_appointment(appointment)
      {:ok, %Appointment{}}

      iex> delete_appointment(appointment)
      {:error, %Ecto.Changeset{}}

  """
  def delete_appointment(%Appointment{} = appointment) do
    Repo.delete(appointment)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking appointment changes.

  ## Examples

      iex> change_appointment(appointment)
      %Ecto.Changeset{data: %Appointment{}}

  """
  def change_appointment(%Appointment{} = appointment, attrs \\ %{}) do
    Appointment.changeset(appointment, attrs)
  end

  alias PetClinic.AppointmentService.ExpertSchedule

  @doc """
  Returns the list of expert_schedules.

  ## Examples

      iex> list_expert_schedules()
      [%ExpertSchedule{}, ...]

  """
  def list_expert_schedules do
    Repo.all(ExpertSchedule)
  end

  @doc """
  Gets a single expert_schedule.

  Raises `Ecto.NoResultsError` if the Expert schedule does not exist.

  ## Examples

      iex> get_expert_schedule!(123)
      %ExpertSchedule{}

      iex> get_expert_schedule!(456)
      ** (Ecto.NoResultsError)

  """
  def get_expert_schedule!(id), do: Repo.get!(ExpertSchedule, id)

  @doc """
  Creates a expert_schedule.

  ## Examples

      iex> create_expert_schedule(%{field: value})
      {:ok, %ExpertSchedule{}}

      iex> create_expert_schedule(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_expert_schedule(attrs \\ %{}) do
    %ExpertSchedule{}
    |> ExpertSchedule.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a expert_schedule.

  ## Examples

      iex> update_expert_schedule(expert_schedule, %{field: new_value})
      {:ok, %ExpertSchedule{}}

      iex> update_expert_schedule(expert_schedule, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_expert_schedule(%ExpertSchedule{} = expert_schedule, attrs) do
    expert_schedule
    |> ExpertSchedule.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a expert_schedule.

  ## Examples

      iex> delete_expert_schedule(expert_schedule)
      {:ok, %ExpertSchedule{}}

      iex> delete_expert_schedule(expert_schedule)
      {:error, %Ecto.Changeset{}}

  """
  def delete_expert_schedule(%ExpertSchedule{} = expert_schedule) do
    Repo.delete(expert_schedule)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking expert_schedule changes.

  ## Examples

      iex> change_expert_schedule(expert_schedule)
      %Ecto.Changeset{data: %ExpertSchedule{}}

  """
  def change_expert_schedule(%ExpertSchedule{} = expert_schedule, attrs \\ %{}) do
    ExpertSchedule.changeset(expert_schedule, attrs)
  end

  def time_range([from | _] = acc, limit, step) do
    nt = Time.add(from, step)

    case Time.compare(nt, limit) do
      :lt ->
        time_range([nt | acc], limit, step)

      _ ->
        Enum.reverse(acc)
    end
  end

  def time_range(from, to, step), do: time_range([from], to, step)

  @doc """
  Get the available appointment slots for the given veterinarian in the given
  time interval
  """
  def available_slots(expert_id, from, to) do
    case Repo.get_by(ExpertSchedule, expert_id: expert_id) do
      nil ->
        {:error, "invalid expert id..."}

      schedule ->
        hour_blocks =
          time_range(schedule.start_hour, schedule.end_hour, 1800)
          |> Enum.map(&Time.truncate(&1, :second))

        appointments =
          Appointment
          |> where([a], a.expert_id == ^expert_id)
          |> select([a], a.datetime)
          |> Repo.all()

        slots =
          max(from, schedule.start_date)
          |> Date.range(min(to, schedule.end_date))
          |> Map.new(fn day ->
            {
              day,
              hour_blocks
              |> Enum.filter(fn hour ->
                DateTime.new!(day, hour) not in appointments
              end)
            }
          end)

        {:ok, slots}
    end
  end

  @doc """
  Tries to schedule a new appointment.
  """
  def new_appointment(expert_id, pet_id, datetime) do
    date = DateTime.to_date(datetime)

    with {:ok, slots} <- available_slots(expert_id, date, date) do
      if DateTime.to_time(datetime) in Map.get(slots, date, []) do
        if is_nil(Repo.get(Pet, pet_id)) do
          {:error, "Invalid pet id..."}
        else
          Repo.insert(%Appointment{
            expert_id: expert_id,
            pet_id: pet_id,
            datetime: datetime
          })
        end
      else
        {:error, "Appointment not available..."}
      end
    end
  end
end
