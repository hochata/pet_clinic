defmodule PetClinic.AppointmentService.Appointment do
  @moduledoc false

  use Ecto.Schema
  import Ecto.Changeset

  alias PetClinic.PetClinicService.{Pet, PetHealthExpert}

  schema "appointments" do
    field :datetime, :utc_datetime
    belongs_to :pet, Pet
    belongs_to :expert, PetHealthExpert

    timestamps()
  end

  @doc false
  def changeset(appointment, attrs) do
    appointment
    |> cast(attrs, [:datetime])
    |> validate_required([:datetime])
  end
end
