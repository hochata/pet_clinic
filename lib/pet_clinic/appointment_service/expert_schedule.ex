defmodule PetClinic.AppointmentService.ExpertSchedule do
  @moduledoc false

  use Ecto.Schema
  import Ecto.Changeset

  alias PetClinic.PetClinicService.PetHealthExpert

  schema "expert_schedules" do
    field :end_hour, :time
    field :start_hour, :time
    field :start_date, :date
    field :end_date, :date

    belongs_to :expert, PetHealthExpert

    timestamps()
  end

  @doc false
  def changeset(expert_schedule, attrs) do
    expert_schedule
    |> cast(attrs, [:end_hour, :start_hour, :end_date, :start_date, :expert_id])
    |> validate_required([:end_hour, :start_hour, :end_date, :start_date, :expert_id])
  end
end
