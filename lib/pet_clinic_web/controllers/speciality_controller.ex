defmodule PetClinicWeb.SpecialityController do
  use PetClinicWeb, :controller

  alias PetClinic.PetClinicService
  alias PetClinic.PetClinicService.Speciality

  def index(conn, _params) do
    specialities = PetClinicService.list_specialities()
    render(conn, "index.html", specialities: specialities)
  end

  def new(conn, _params) do
    changeset = PetClinicService.change_speciality(%Speciality{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"speciality" => speciality_params}) do
    case PetClinicService.create_speciality(speciality_params) do
      {:ok, speciality} ->
        conn
        |> put_flash(:info, "Speciality created successfully.")
        |> redirect(to: Routes.speciality_path(conn, :show, speciality))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    speciality = PetClinicService.get_speciality!(id)
    render(conn, "show.html", speciality: speciality)
  end

  def edit(conn, %{"id" => id}) do
    speciality = PetClinicService.get_speciality!(id)
    changeset = PetClinicService.change_speciality(speciality)
    render(conn, "edit.html", speciality: speciality, changeset: changeset)
  end

  def update(conn, %{"id" => id, "speciality" => speciality_params}) do
    speciality = PetClinicService.get_speciality!(id)

    case PetClinicService.update_speciality(speciality, speciality_params) do
      {:ok, speciality} ->
        conn
        |> put_flash(:info, "Speciality updated successfully.")
        |> redirect(to: Routes.speciality_path(conn, :show, speciality))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", speciality: speciality, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    speciality = PetClinicService.get_speciality!(id)
    {:ok, _speciality} = PetClinicService.delete_speciality(speciality)

    conn
    |> put_flash(:info, "Speciality deleted successfully.")
    |> redirect(to: Routes.speciality_path(conn, :index))
  end
end
