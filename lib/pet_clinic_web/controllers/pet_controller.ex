defmodule PetClinicWeb.PetController do
  use PetClinicWeb, :controller

  alias PetClinic.PetClinicService
  alias PetClinic.PetClinicService.Pet

  def index(conn, _params) do
    pets = PetClinicService.list_pets(preloads: [:species, :owner, :preferred_expert])
    render(conn, "index.html", pets: pets)
  end

  def new(conn, _params) do
    changeset = PetClinicService.change_pet(%Pet{})

    species =
      PetClinicService.list_species()
      |> Map.new(fn %{id: id, name: name} -> {name, id} end)

    owners =
      PetClinicService.list_owners()
      |> Map.new(fn %{id: id, name: name} -> {name, id} end)

    health_experts =
      PetClinicService.list_pet_health_experts()
      |> Map.new(fn %{id: id, name: name} -> {name, id} end)

    render(
      conn,
      "new.html",
      species: species,
      owners: owners,
      health_experts: health_experts,
      changeset: changeset
    )
  end

  def create(conn, %{"pet" => pet_params}) do
    species =
      PetClinicService.list_species()
      |> Map.new(fn %{id: id, name: name} -> {name, id} end)

    owners =
      PetClinicService.list_owners()
      |> Map.new(fn %{id: id, name: name} -> {name, id} end)

    health_experts =
      PetClinicService.list_pet_health_experts()
      |> Map.new(fn %{id: id, name: name} -> {name, id} end)

    case PetClinicService.create_pet(pet_params) do
      {:ok, pet} ->
        conn
        |> put_flash(:info, "Pet created successfully.")
        |> redirect(to: Routes.pet_path(conn, :show, pet))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(
          conn,
          "new.html",
          species: species,
          owners: owners,
          health_experts: health_experts,
          changeset: changeset
        )
    end
  end

  def show(conn, %{"id" => id}) do
    pet =
      PetClinicService.get_pet!(
        id,
        preloads: [:species, :owner, :preferred_expert]
      )

    render(conn, "show.html", pet: pet)
  end

  def edit(conn, %{"id" => id}) do
    pet =
      PetClinicService.get_pet!(
        id,
        preloads: [:species, :owner, :preferred_expert]
      )

    species =
      PetClinicService.list_species()
      |> Map.new(fn %{id: id, name: name} -> {name, id} end)

    owners =
      PetClinicService.list_owners()
      |> Map.new(fn %{id: id, name: name} -> {name, id} end)

    health_experts =
      PetClinicService.list_pet_health_experts()
      |> Map.new(fn %{id: id, name: name} -> {name, id} end)

    changeset = PetClinicService.change_pet(pet)

    render(
      conn,
      "edit.html",
      pet: pet,
      species: species,
      owners: owners,
      health_experts: health_experts,
      changeset: changeset
    )
  end

  def update(conn, %{"id" => id, "pet" => pet_params}) do
    pet = PetClinicService.get_pet!(id, preloads: :species)

    species =
      PetClinicService.list_species()
      |> Map.new(fn %{id: id, name: name} -> {name, id} end)

    owners =
      PetClinicService.list_owners()
      |> Map.new(fn %{id: id, name: name} -> {name, id} end)

    health_experts =
      PetClinicService.list_pet_health_experts()
      |> Map.new(fn %{id: id, name: name} -> {name, id} end)

    case PetClinicService.update_pet(pet, pet_params) do
      {:ok, pet} ->
        conn
        |> put_flash(:info, "Pet updated successfully.")
        |> redirect(to: Routes.pet_path(conn, :show, pet))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(
          conn,
          "edit.html",
          species: species,
          owners: owners,
          health_experts: health_experts,
          pet: pet,
          changeset: changeset
        )
    end
  end

  def delete(conn, %{"id" => id}) do
    pet = PetClinicService.get_pet!(id)
    {:ok, _pet} = PetClinicService.delete_pet(pet)

    conn
    |> put_flash(:info, "Pet deleted successfully.")
    |> redirect(to: Routes.pet_path(conn, :index))
  end
end
