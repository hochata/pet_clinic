defmodule PetClinicWeb.PetHealthExpertController do
  use PetClinicWeb, :controller

  alias PetClinic.PetClinicService
  alias PetClinic.AppointmentService
  alias PetClinic.PetClinicService.PetHealthExpert

  def index(conn, _params) do
    pet_health_experts = PetClinicService.list_pet_health_experts(preloads: :specialities)
    render(conn, "index.html", pet_health_experts: pet_health_experts)
  end

  def new(conn, _params) do
    changeset = PetClinicService.change_pet_health_expert(%PetHealthExpert{})

    species =
      PetClinicService.list_species()
      |> Map.new(fn %{id: id, name: name} -> {name, id} end)

    render(conn, "new.html", species: species, changeset: changeset)
  end

  def create(conn, %{"pet_health_expert" => pet_health_expert_params}) do
    species =
      PetClinicService.list_species()
      |> Map.new(fn %{id: id, name: name} -> {name, id} end)

    case PetClinicService.create_pet_health_expert(pet_health_expert_params) do
      {:ok, pet_health_expert} ->
        conn
        |> put_flash(:info, "Pet health expert created successfully.")
        |> redirect(
          to:
            Routes.pet_health_expert_path(
              conn,
              :show,
              pet_health_expert
            )
        )

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", species: species, changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    pet_health_expert =
      PetClinicService.get_pet_health_expert!(
        id,
        preloads: :specialities
      )

    render(conn, "show.html", pet_health_expert: pet_health_expert)
  end

  def show_appointments(conn, %{"id" => id, "date" => date_str}) do
    date = Date.from_iso8601!(date_str)

    aps =
      AppointmentService.get_appointments_by(
        %{expert_id: id, date: date},
        preloads: :pet
      )

    render(conn, "show_appointments.html", appointments: aps)
  end

  def edit(conn, %{"id" => id}) do
    pet_health_expert =
      PetClinicService.get_pet_health_expert!(
        id,
        preloads: :specialities
      )

    changeset = PetClinicService.change_pet_health_expert(pet_health_expert)

    species =
      PetClinicService.list_species()
      |> Map.new(fn %{id: id, name: name} -> {name, id} end)

    render(
      conn,
      "edit.html",
      pet_health_expert: pet_health_expert,
      species: species,
      changeset: changeset
    )
  end

  def update(conn, %{"id" => id, "pet_health_expert" => pet_health_expert_params}) do
    pet_health_expert =
      PetClinicService.get_pet_health_expert!(
        id,
        preloads: :specialities
      )

    species =
      PetClinicService.list_species()
      |> Map.new(fn %{id: id, name: name} -> {name, id} end)

    case PetClinicService.update_pet_health_expert(pet_health_expert, pet_health_expert_params) do
      {:ok, pet_health_expert} ->
        conn
        |> put_flash(:info, "Pet health expert updated successfully.")
        |> redirect(to: Routes.pet_health_expert_path(conn, :show, pet_health_expert))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(
          conn,
          "edit.html",
          species: species,
          pet_health_expert: pet_health_expert,
          changeset: changeset
        )
    end
  end

  def delete(conn, %{"id" => id}) do
    pet_health_expert = PetClinicService.get_pet_health_expert!(id)
    {:ok, _pet_health_expert} = PetClinicService.delete_pet_health_expert(pet_health_expert)

    conn
    |> put_flash(:info, "Pet health expert deleted successfully.")
    |> redirect(to: Routes.pet_health_expert_path(conn, :index))
  end
end
