# PetClinic

Keep track of the pets and veterinarians on your clinic!

## Some nice features

### Cute presentation cards

For pets

![bonnie](./assets/bonnie.png)

For veterinarians

![agus](./assets/agus.png)

### List pets by type

![snakes](./assets/snakes.png)

## Start Server

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.setup`
  * Start Phoenix endpoint with `mix phx.server` or inside IEx with `iex -S mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser!
